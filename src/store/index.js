import { createStore } from 'vuex'

export default createStore({
  state: {
    Products: [],
    AddedItems: []
  },
  getters: {
    GetProducts: state => state.Products,
    GetAddedItems: state => state.AddedItems,
    GetCount: state => state.AddedItems.length ? state.AddedItems.map(item => item.count).reduce((prev, next) => prev + next) : 0,
    GetSum: state => state.AddedItems.length
      ? state.AddedItems
        .map(item => item.count * state.Products.find(product => item.id === product.id).price)
        .reduce((prev, next) => prev + next)
      : 0
  },
  mutations: {
    SetAddItem: (state, id) => {
      const item = state.AddedItems.find(item => id === item.id)
      item ? item.count++ : state.AddedItems.push({ id: id, count: 1 })
    },
    SetRemoveItem: (state, id) => {
      const item = state.AddedItems.find(item => id === item.id)
      item.count > 1 ? item.count-- : state.AddedItems = state.AddedItems.filter(item => id !== item.id)
    },
    SetAddProducts: (state, products) => {
      state.Products.push(...products)
    }
  }
})
